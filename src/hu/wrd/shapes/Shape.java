package hu.wrd.shapes;

import java.util.List;

public interface Shape {
    double getArea();
    List<Shape> sameShapes();
}
