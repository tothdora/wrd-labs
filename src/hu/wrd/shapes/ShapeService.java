package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ShapeService {

    private List<Shape> shapeList = new ArrayList<Shape>();
    private List<Double> areaList = new ArrayList<Double>();


    public void addShapes(Shape ... shapes) {
        //TODO implement
        List<Shape> shapeListBef = new ArrayList<Shape>();
        shapeListBef = shapeList;
        List<Double> areaListBef = new ArrayList<Double>();
        areaListBef = areaList;

        List<Shape> sameShapes = new ArrayList<Shape>();
        for (Shape shape : shapes) {

            if (shape.sameShapes() != null) {

                sameShapes = shape.sameShapes();

                for (int i = sameShapes.size() - 1; i >= 0; i--) {
                    if (!shapeListBef.contains(sameShapes.get(i))) {
                        shapeListBef.add(shape);
                        if (!areaListBef.contains(shape.getArea())) {
                            areaListBef.add(shape.getArea());
                        }
                    } else {
                        return;
                    }
                }

            } else {
                shapeListBef.add(shape);
                areaListBef.add(shape.getArea());
            }
        }
        shapeList = shapeListBef;
        areaList = areaListBef;


    }

    public void printShapesOrderByAreaAsc() {
        //TODO implement
        System.out.println("printShapesOrderByAreaAsc() called");
        System.out.println();
        Collections.sort(areaList, Collections.reverseOrder());
        Iterator<Double> iterArea = areaList.iterator();
        Iterator<Shape> iter = shapeList.iterator();

        for (int i = areaList.size() - 1; i >= 0; i-- ) {
            for (int j = shapeList.size() - 1; j >= 0; j--) {
                if(shapeList.get(j).getArea() == areaList.get(i)) {
                    System.out.println(shapeList.get(j).toString());
                }
            }

        }

    }

    public void printShapesOrderByAreaDesc() {
        //TODO implement
        System.out.println("printShapesOrderByAreaAsc() called");
        System.out.println();
        Collections.sort(areaList);
        Iterator<Double> iterArea = areaList.iterator();
        Iterator<Shape> iter = shapeList.iterator();

        for (int i = areaList.size() - 1; i >= 0; i-- ) {
            for (int j = shapeList.size() - 1; j >= 0; j--) {
                if(shapeList.get(j).getArea() == areaList.get(i)) {
                    System.out.println(shapeList.get(j).toString());
                }
            }

        }
    }
}
