package hu.wrd.shapes;

import com.sun.source.tree.BreakTree;

import java.util.ArrayList;
import java.util.List;

public class Circle implements Shape {
    private int rad;
    Circle(int radius) {
        if(radius > 0)
            this.rad = radius;
        else
            System.out.println("rad must be positive and not 0");
    }

    public double getArea() {
        return Math.PI*(this.rad*this.rad);
    }

    public List<Shape> sameShapes() {
        return null;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("rad=").append(rad);
        sb.append('}');
        return sb.toString();
    }
}
