package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.List;

public class Rectangle implements Shape {

    private int x, y;
    Rectangle(int x, int y) {
        if( x > 0 && y > 0) {
            this.x = x;
            this.y = y;
        } else {
            System.out.println("x and y must be positive and not 0");
        }
    }

    public List<Shape> sameShapes() {
        List<Shape> shapes = new ArrayList<Shape>();
        shapes.add(new Rectangle(this.y, this.x));
        if(this.x == this.y) {
            shapes.add(new Square(x));
        }
        return shapes;
    }

    public double getArea() {
        return this.x * this.y;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("x=").append(x);
        sb.append(", y=").append(y);
        sb.append('}');
        return sb.toString();
    }
}
