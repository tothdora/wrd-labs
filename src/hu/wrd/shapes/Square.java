package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.List;

public class Square implements Shape {

    private int x;

    Square(int x) {
        if(x > 0)
            this.x = x;
        else
            System.out.println("x must be positive and not 0");
    }
    public List<Shape> sameShapes() {
        List<Shape> shapes = new ArrayList<Shape>();
        shapes.add(new Rectangle(this.x, this.x));
        return shapes;
    }
    public double getArea() {
        return this.x * this.x;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Square{");
        sb.append("x=").append(x);
        sb.append('}');
        return sb.toString();
    }
}
